-- phpMyAdmin SQL Dump
-- version 4.1.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mag 31, 2016 alle 14:27
-- Versione del server: 5.1.71-community-log
-- PHP Version: 5.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `my_scrum97`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `T_AULA`
--

CREATE TABLE IF NOT EXISTS `T_AULA` (
  `ID_AULA` int(11) NOT NULL AUTO_INCREMENT,
  `AULA` text NOT NULL,
  `POSTI_TOTALI` int(11) NOT NULL,
  PRIMARY KEY (`ID_AULA`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dump dei dati per la tabella `T_AULA`
--

INSERT INTO `T_AULA` (`ID_AULA`, `AULA`, `POSTI_TOTALI`) VALUES
(1, 'aula_magna', 100),
(2, 'aula_video', 50),
(3, 'Aula_teal', 25);

-- --------------------------------------------------------

--
-- Struttura della tabella `t_aulaeventi`
--

CREATE TABLE IF NOT EXISTS `t_aulaeventi` (
  `id_aulaeventi` int(11) NOT NULL AUTO_INCREMENT,
  `id_aula` int(11) NOT NULL,
  `id_evento` int(11) NOT NULL,
  `POSTI_PREN` int(11) NOT NULL,
  `POSTI_TOT` int(11) NOT NULL,
  PRIMARY KEY (`id_aulaeventi`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `T_CLASSI`
--

CREATE TABLE IF NOT EXISTS `T_CLASSI` (
  `CLASSE` varchar(20) NOT NULL,
  `NUMERO_ALUNNI` int(20) NOT NULL,
  UNIQUE KEY `CLASSE` (`CLASSE`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `T_CLASSI`
--

INSERT INTO `T_CLASSI` (`CLASSE`, `NUMERO_ALUNNI`) VALUES
('5C', 13),
('5A', 16);

-- --------------------------------------------------------

--
-- Struttura della tabella `T_DOCENTI`
--

CREATE TABLE IF NOT EXISTS `T_DOCENTI` (
  `ID_DOCENTE` int(11) NOT NULL AUTO_INCREMENT,
  `NOME` text NOT NULL,
  `COGNOME` text NOT NULL,
  `USER` text NOT NULL,
  `PSW` text NOT NULL,
  PRIMARY KEY (`ID_DOCENTE`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dump dei dati per la tabella `T_DOCENTI`
--

INSERT INTO `T_DOCENTI` (`ID_DOCENTE`, `NOME`, `COGNOME`, `USER`, `PSW`) VALUES
(1, 'rossi', '', 'rossi90\r\n', '123'),
(2, 'booo', '', 'booo123', '1234'),
(3, '', '', 'DANI', '1234');

-- --------------------------------------------------------

--
-- Struttura della tabella `T_DOCENTICLASSI`
--

CREATE TABLE IF NOT EXISTS `T_DOCENTICLASSI` (
  `ID_DOCENTICLASSE` int(11) NOT NULL AUTO_INCREMENT,
  `ID_DOCENTE` int(11) NOT NULL,
  `ID_CLASSE` int(11) NOT NULL,
  PRIMARY KEY (`ID_DOCENTICLASSE`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `T_EVENTI`
--

CREATE TABLE IF NOT EXISTS `T_EVENTI` (
  `ID_EVENTO` int(11) NOT NULL AUTO_INCREMENT,
  `EVENTO` text NOT NULL,
  `DESCRIZIONE` text NOT NULL,
  PRIMARY KEY (`ID_EVENTO`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dump dei dati per la tabella `T_EVENTI`
--

INSERT INTO `T_EVENTI` (`ID_EVENTO`, `EVENTO`, `DESCRIZIONE`) VALUES
(1, 'Assemblea d''Istituto', 'MOMENTO PER SCAMBIARSI PARERI'),
(2, 'incontro università', 'CHIARIMENTI SULLE PERPLESSITA'' DEI RAGAZZI RIGUARDO AL LORO FUTURO'),
(3, 'Incontro Terrorismo', 'SPIEGAZIONE DI QUELLO CHE E'' ACCADUTO E STA ACCADENDO INTORNO A NOI'),
(4, 'Incontro R.I.S ', 'INCONTRO INTERESSANTE SULLE TECNICHE PRATICHE DI INDAGINE NEI LUOGHI DEL REATO '),
(5, 'assemblea', 'ciao cioa');

-- --------------------------------------------------------

--
-- Struttura della tabella `T_PRENOTAZIONE`
--

CREATE TABLE IF NOT EXISTS `T_PRENOTAZIONE` (
  `ID_PRENOTAZIONE` int(11) NOT NULL AUTO_INCREMENT,
  `ID_DOCENTE` int(11) NOT NULL,
  `ID_AULA` int(11) NOT NULL,
  `ID_EVENTO` int(11) NOT NULL,
  `DATA` date NOT NULL,
  `ORA_INIZIO` time NOT NULL,
  `ORA_FINE` varchar(30) NOT NULL,
  `CLASSE` varchar(20) NOT NULL,
  `POSTI_PREN` int(11) NOT NULL,
  PRIMARY KEY (`ID_PRENOTAZIONE`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Dump dei dati per la tabella `T_PRENOTAZIONE`
--

INSERT INTO `T_PRENOTAZIONE` (`ID_PRENOTAZIONE`, `ID_DOCENTE`, `ID_AULA`, `ID_EVENTO`, `DATA`, `ORA_INIZIO`, `ORA_FINE`, `CLASSE`, `POSTI_PREN`) VALUES
(1, 0, 0, 0, '0000-00-00', '00:00:00', '', '', 0),
(2, 0, 0, 0, '0000-00-00', '00:00:00', '', '', 0),
(3, 0, 0, 0, '0000-00-00', '00:00:00', '', '', 0),
(4, 0, 1, 1, '2016-05-11', '11:01:00', '12:00', '5C', 20),
(5, 0, 1, 1, '0000-00-00', '00:00:00', '', '5C', 0),
(6, 0, 1, 1, '0000-00-00', '00:00:00', '', '5C', 0),
(7, 0, 1, 3, '2016-12-15', '08:00:00', '13:00', '5C', 13),
(8, 0, 0, 0, '0000-00-00', '00:00:00', '', '', 0),
(9, 0, 1, 1, '0000-00-00', '00:00:00', '', '5C', 0),
(10, 0, 1, 1, '0000-00-00', '00:00:00', '', '5C', 0),
(11, 0, 0, 0, '0000-00-00', '00:00:00', '', '', 0),
(12, 0, 0, 0, '0000-00-00', '00:00:00', '', '', 0),
(13, 0, 1, 1, '0000-00-00', '00:00:00', '', '5C', 0),
(14, 0, 1, 1, '0000-00-00', '00:00:00', '', '5C', 0),
(15, 0, 1, 1, '2016-11-11', '11:11:00', '13:11', '5C', 32),
(16, 0, 0, 0, '0000-00-00', '00:00:00', '', '', 0),
(17, 0, 0, 0, '0000-00-00', '00:00:00', '', '', 0),
(18, 0, 0, 0, '0000-00-00', '00:00:00', '', '', 0),
(19, 0, 0, 0, '0000-00-00', '00:00:00', '', '', 0),
(20, 0, 0, 0, '0000-00-00', '00:00:00', '', '', 0),
(21, 0, 0, 0, '0000-00-00', '00:00:00', '', '', 0),
(22, 0, 1, 1, '0000-00-00', '23:05:00', '05:32', '5C', 234),
(23, 0, 1, 1, '0000-00-00', '00:00:00', '', '5C', 0),
(24, 0, 1, 1, '0424-03-21', '03:24:00', '23:43', '5A', 34),
(25, 0, 1, 1, '0235-04-23', '03:56:00', '06:47', '5C', 899),
(26, 0, 1, 1, '0000-00-00', '00:00:00', '', '5C', 0),
(27, 0, 2, 2, '2016-05-02', '08:00:00', '13:00', '5C', 13),
(28, 0, 1, 1, '2016-02-03', '08:00:00', '13:15', '5C', 13),
(29, 0, 1, 1, '0000-00-00', '00:00:00', '', '5C', 0),
(30, 0, 1, 1, '0000-00-00', '00:00:00', '', '5C', 0),
(31, 0, 1, 1, '2016-05-06', '13:06:00', '13:07', '5C', 12),
(32, 0, 1, 1, '2016-05-06', '12:07:00', '14:07', '5C', 1),
(33, 0, 0, 0, '0000-00-00', '00:00:00', '', '', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
