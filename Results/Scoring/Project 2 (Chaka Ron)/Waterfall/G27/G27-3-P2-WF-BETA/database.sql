create table Conversation (
    ID int(10) primary key,
    title varchar(255) not null,
    day timestamp,
    UserID int(10) not null,
    foreign key(UserID) references User(ID))

create table User (
    ID int(10) primary key,
    name varchar(50),
    password varchar(16),
    last_logged timestamp not null,
    isAdmin bit,
    totalreputation int(10)
    )

create table Micropost (
    ID int(10) primary key,
    text varchar(160),
    totalvotes int(10),
    date timestamp,
    ConversationID int(10),
    foreign key(ConversationID) references Conversation(ID))

create table Micropost_User (
    MicropostID int(10),
    UserID int(10),
    positive bit,
    foreign key(MicropostID) references Micropost(ID),
    foreign key(UserID) references User(ID)
    )