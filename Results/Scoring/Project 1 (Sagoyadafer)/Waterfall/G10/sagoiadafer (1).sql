-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Creato il: Apr 07, 2016 alle 21:21
-- Versione del server: 10.1.9-MariaDB
-- Versione PHP: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sagoiadafer`
--
CREATE DATABASE IF NOT EXISTS `sagoiadafer` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `sagoiadafer`;

-- --------------------------------------------------------

--
-- Struttura della tabella `color`
--

CREATE TABLE `color` (
  `ID` int(11) NOT NULL,
  `name` varchar(15) DEFAULT NULL,
  `HTML` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `label`
--

CREATE TABLE `label` (
  `ID` int(10) NOT NULL,
  `name` int(10) DEFAULT NULL,
  `UserID` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `label_reminder`
--

CREATE TABLE `label_reminder` (
  `LabelID` int(10) NOT NULL,
  `ReminderID` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `registered_user`
--

CREATE TABLE `registered_user` (
  `ID` int(10) NOT NULL,
  `name` varchar(15) NOT NULL,
  `psw` varchar(16) NOT NULL,
  `last_logged` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `registered_user`
--

INSERT INTO `registered_user` (`ID`, `name`, `psw`, `last_logged`, `email`) VALUES
(1, 'admin', 'Admin12345', '31/03/16 12:08:21', 'admin@admin.it'),
(2, 'andrea', 'Andrea123', '31/03/16 12:28:55', 'fevdfb'),
(3, 'matti', 'Mattia123', '31/03/16 12:31:14', 'lodi1997@live.it'),
(4, 'Giorgia', 'Giorgia123', '', 'njkbnkjb'),
(5, 'simone', 'Simone123', '31/03/16 12:46:16', 'jknsvdndsvlksdv');

-- --------------------------------------------------------

--
-- Struttura della tabella `reminder`
--

CREATE TABLE `reminder` (
  `ID` int(11) NOT NULL,
  `title` varchar(25) NOT NULL,
  `descr` varchar(2500) DEFAULT NULL,
  `done` bit(1) NOT NULL,
  `priority` int(10) DEFAULT NULL,
  `end_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `UserID` int(10) DEFAULT NULL,
  `ColorID` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `reminder`
--

INSERT INTO `reminder` (`ID`, `title`, `descr`, `done`, `priority`, `end_date`, `UserID`, `ColorID`) VALUES
(1, '', NULL, b'0', 0, '2016-03-31 11:24:07', NULL, NULL);

-- --------------------------------------------------------

--
-- Struttura della tabella `site_settings`
--

CREATE TABLE `site_settings` (
  `AdsURL` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `users`
--

CREATE TABLE `users` (
  `ID` int(11) NOT NULL,
  `Colonna` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `color`
--
ALTER TABLE `color`
  ADD PRIMARY KEY (`ID`);

--
-- Indici per le tabelle `label`
--
ALTER TABLE `label`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `UserID` (`UserID`);

--
-- Indici per le tabelle `label_reminder`
--
ALTER TABLE `label_reminder`
  ADD KEY `LabelID` (`LabelID`),
  ADD KEY `ReminderID` (`ReminderID`);

--
-- Indici per le tabelle `registered_user`
--
ALTER TABLE `registered_user`
  ADD PRIMARY KEY (`ID`);

--
-- Indici per le tabelle `reminder`
--
ALTER TABLE `reminder`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `UserID` (`UserID`),
  ADD KEY `ColorID` (`ColorID`);

--
-- Indici per le tabelle `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `color`
--
ALTER TABLE `color`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `label`
--
ALTER TABLE `label`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `registered_user`
--
ALTER TABLE `registered_user`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT per la tabella `reminder`
--
ALTER TABLE `reminder`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT per la tabella `users`
--
ALTER TABLE `users`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `label`
--
ALTER TABLE `label`
  ADD CONSTRAINT `label_ibfk_1` FOREIGN KEY (`UserID`) REFERENCES `users` (`ID`);

--
-- Limiti per la tabella `label_reminder`
--
ALTER TABLE `label_reminder`
  ADD CONSTRAINT `label_reminder_ibfk_1` FOREIGN KEY (`LabelID`) REFERENCES `label` (`ID`),
  ADD CONSTRAINT `label_reminder_ibfk_2` FOREIGN KEY (`ReminderID`) REFERENCES `reminder` (`ID`);

--
-- Limiti per la tabella `reminder`
--
ALTER TABLE `reminder`
  ADD CONSTRAINT `reminder_ibfk_1` FOREIGN KEY (`UserID`) REFERENCES `users` (`ID`),
  ADD CONSTRAINT `reminder_ibfk_2` FOREIGN KEY (`ColorID`) REFERENCES `color` (`ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
