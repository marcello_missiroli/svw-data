-- phpMyAdmin SQL Dump
-- version 4.1.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mag 04, 2016 alle 13:38
-- Versione del server: 5.1.71-community-log
-- PHP Version: 5.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `my_lorenzoalberetti`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `Color`
--

CREATE TABLE IF NOT EXISTS `Color` (
  `ID` int(10) NOT NULL,
  `name` varchar(15) DEFAULT NULL,
  `HTML` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `Label`
--

CREATE TABLE IF NOT EXISTS `Label` (
  `ID` int(10) NOT NULL,
  `name` int(10) DEFAULT NULL,
  `UserID` int(10) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `UserID` (`UserID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `Label_Reminder`
--

CREATE TABLE IF NOT EXISTS `Label_Reminder` (
  `LabelID` int(10) NOT NULL DEFAULT '0',
  `ReminderID` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`LabelID`,`ReminderID`),
  KEY `ReminderID` (`ReminderID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `RegisteredUser`
--

CREATE TABLE IF NOT EXISTS `RegisteredUser` (
  `name` varchar(50) DEFAULT NULL,
  `password` varchar(16) DEFAULT NULL,
  `UserID` int(10) DEFAULT NULL,
  `last_logged` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `UserID` (`UserID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `Reminder`
--

CREATE TABLE IF NOT EXISTS `Reminder` (
  `ID` int(10) NOT NULL,
  `title` varchar(25) DEFAULT NULL,
  `descr` varchar(2500) DEFAULT NULL,
  `done` bit(1) DEFAULT NULL,
  `priority` int(10) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `UserID` int(10) DEFAULT NULL,
  `Color` int(10) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `UserID` (`UserID`),
  KEY `Color` (`Color`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `SiteSettings`
--

CREATE TABLE IF NOT EXISTS `SiteSettings` (
  `AdsURL` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `User`
--

CREATE TABLE IF NOT EXISTS `User` (
  `ID` int(10) NOT NULL,
  `Column` int(10) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
