-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generato il: Apr 27, 2016 alle 09:24
-- Versione del server: 5.5.34
-- Versione PHP: 5.4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `promemoria_db`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `memo`
--

CREATE TABLE IF NOT EXISTS `memo` (
  `Username` text NOT NULL,
  `password` text NOT NULL,
  `email` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(16) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(60) NOT NULL,
  `reg_ip` varchar(20) DEFAULT NULL,
  `last_ip` varchar(20) DEFAULT NULL,
  `reg_date` int(11) NOT NULL,
  `last_login` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dump dei dati per la tabella `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `reg_ip`, `last_ip`, `reg_date`, `last_login`) VALUES
(1, 'ciao', '6e6bc4e49dd477ebc98ef4046c067b5f', 'giuseppeorilia@live.it', '::1', '::1', 1461321994, NULL),
(2, 'peppe', '6c79f0f8b1b095ef10ed38a1ee445aa7', 'giovanni@live.it', '::1', '::1', 1461322178, NULL),
(3, 'gigi', 'e47ca7a09cf6781e29634502345930a7', 'computerlab@hotmail.it', '::1', '::1', 1461322429, NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
