Nicoletta Rossi June 6,2016
* How would you define your knowledges on agile methodologies? And how did you acquire them?
Sufficient. Mostly acquired online and by doing this experience.
* How would you see agile methodologies in a daily teaching routine?
It really depends on the class at hand. However, in general, since I discovered Agile I think it is the way to go.
* Do you think this experiment was useful in theaching the student the basics of both projectual methodologies?
I think it worked for the agile methdod, but they didn't really get the waterfall system.
* Was the experiment well designed? Times were OK?
Time was ok, but they already had previous experience with similar activities, in particular for the agile one.
* Did the experiment have some long-term effect on the class? Grades, motivation, self-esteem...
I can't tell.
* During the execution, did they stick to the role they chose or were given?
Yes, they did, especially in the waterfall case.
* Any particular fact that happened during the activity which is worth mentioning?
The Agile group, though having potentiall better studing, had problem finding an internal balance, often arguing. The others immediatly accepted the leader role and everything went smoothly. It is really difficult to predict group dynamics. 
* Final comments?
The most interesting fact has been that everybody worked with engagement for the full duration, that rarely happens. Moreover, it ist difficult to change and experiment new stuff if you have to start from scratch; in this case, having so much material ready really helped in deciding to do it. Next year I'll try it with younger students. 
